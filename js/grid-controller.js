"use strict";
grid_switch.addEventListener('click', setupGrid);
let gridContainer = document.querySelector('.stock-grid');
let searchInput;
let changed;
let searchButton;
setupGrid();

function setupGrid()
{
        document.querySelector('.stock-list').innerHTML = '';
        grid_switch.parentElement.classList.add('active');
        list_switch.parentElement.classList.remove('active');

        document.querySelector('.misc-controls').innerHTML = '<input type="text" placeholder="Search by ingredient..." maxlength="20" class="search">\n' +
            '                        <button class="search-submit"><i class="fas fa-search"></i></button>';

        searchButton = document.querySelector('.search-submit');
        searchInput = document.querySelector('.search');
        changed = false;

        searchButton.onclick = searchIngredient;
        searchInput.addEventListener('change', gridReinit);

        gridInit();
}

function gridInit() {
    for (let i=0; i<pizzaMenu.length; i++)
    {
        let card = document.createElement('div');
        card.classList.add('card');
        card.id = 'card_'+i;

        let photo = document.createElement('div');
        photo.classList.add('card-photo');
        photo.style.backgroundImage = `url(${pizzaMenu[i].photo_name})`;


        let header = document.createElement('h3');
        header.classList.add('card-header');
        header.innerHTML = `${pizzaMenu[i].name}`;

        let hr = document.createElement('hr');
        hr.classList.add('card-hr');

        let ingredients = document.createElement('div');
        ingredients.classList.add('card-ingredients');
        let text = 'Ingredients: ';

        let arr = Array.from(pizzaMenu[i].ingredients);
        for (let i=0; i<arr.length; i++)
        {
                text += arr[i] + ', ';
        }
        ingredients.innerHTML = text.slice(0,text.length-2);

        let info = document.createElement('div');
        info.classList.add('card-info');
        let price = document.createElement('div');
        price.classList.add('price');
        price.innerHTML = `<i class="fas fa-dollar-sign"></i> ${pizzaMenu[i].price}`;
        info.appendChild(price);
        let calorie = document.createElement('div');
        calorie.classList.add('calories');
        calorie.innerHTML = `<i class="fas fa-pizza-slice"></i> ${pizzaMenu[i].calories} cal<span class="small">/100g</span>`;
        info.appendChild(calorie);

        let buttonWrap = document.createElement('div');
        buttonWrap.classList.add('card-button-wrap');
        let details = document.createElement('button');
        details.classList.add('pizza-button');
        details.innerHTML = `Details <i class="fas fa-ellipsis-h"></i>`;
        buttonWrap.appendChild(details);
        let cart = document.createElement('button');
        cart.classList.add('pizza-button');
        cart.classList.add('cart-button');
        cart.innerHTML = `Add to cart <i class="fas fa-cart-plus"></i>`;
        buttonWrap.appendChild(cart);

        card.appendChild(photo);
        card.appendChild(header);
        card.appendChild(hr);
        card.appendChild(ingredients);
        card.appendChild(info);
        card.appendChild(buttonWrap);

        gridContainer.appendChild(card);
    }
}


function searchIngredient() {
    let str = searchInput.value;
    if (!ingredientStock.has(str.toLowerCase())) {alert('No such ingredient!');}
    else
    {
        for (let i=0; i<pizzaMenu.length; i++)
        {
            if(!pizzaMenu[i].ingredients.has(str.toLowerCase()))
            {
                gridContainer.removeChild(document.getElementById('card_'+i));
                changed = true;
            }
        }
    }
}
function gridReinit() {
    if (searchInput.value === '' && changed)
    {
        gridContainer.innerHTML = '';
        gridInit();
        changed = false;
    }
}