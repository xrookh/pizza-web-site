"use strict";
list_switch.addEventListener('click', setupList);
let listContainer = document.querySelector('.stock-list');
let pizzaList;
let select;
function setupList() {
    document.querySelector('.stock-grid').innerHTML = '';
    list_switch.parentElement.classList.add('active');
    grid_switch.parentElement.classList.remove('active');
    pizzaList = [...pizzaMenu];
    document.querySelector('.misc-controls').innerHTML = '<select name="sort-options" id="" class="sort-options">\n' +
        '                            <optgroup label="Sort by:">\n' +
        '                                <option value="name">Name</option>\n' +
        '                                <option value="price-d">Price descending</option>\n' +
        '                                <option value="price-a">Price ascending</option>\n' +
        '                            </optgroup>\n' +
        '                        </select>';

    select = document.querySelector('.sort-options');
    select.addEventListener('change', sortList);
    listInit();
}

function listInit() {
    listContainer.innerHTML = '';

    for (let i=0; i<pizzaList.length; i++)
    {
        let listItem = document.createElement('div');
        listItem.classList.add('list-item');

        let name = document.createElement('div');
        name.classList.add('list-name');
        name.innerHTML = `<i class="fas fa-pizza-slice list-icon"></i> &nbsp;${pizzaList[i].name}`;

        let price = document.createElement('div');
        price.classList.add('list-price');
        price.innerHTML = `<i class="fas fa-dollar-sign list-dollar"></i>&nbsp;${pizzaList[i].price}`;

        let buttons = document.createElement('div');
        buttons.classList.add('list-buttons');
        let details = document.createElement('button');
        details.classList.add('list-details');
        details.innerHTML = '<i class="fas fa-ellipsis-h"></i>';
        let cart = document.createElement('button');
        cart.classList.add('list-cart');
        cart.innerHTML = '<i class="fas fa-cart-plus"></i>';
        buttons.appendChild(details);
        buttons.appendChild(cart);

        listItem.appendChild(name);
        listItem.appendChild(price);
        listItem.appendChild(buttons);

        listContainer.appendChild(listItem);
    }
}

function sortList()
{
    if (select.value === 'name')
    {
        pizzaList.sort(sortListByName);
        listInit();
    } else if (select.value === 'price-a')
    {
        pizzaList.sort(sortListByPriceA);
        listInit();
    } else
    {
        pizzaList.sort(sortListByPriceD);
        listInit();
    }
}

function sortListByName(a, b)
{
    if (a.name > b.name) return 1; else return -1
}
function sortListByPriceD(a,b) {
    if (a.price < b.price) return 1; else return -1;
}
function sortListByPriceA(a,b) {
    if (a.price > b.price) return 1; else return -1;
}