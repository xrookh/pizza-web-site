"use strict";
let ingredientStock = new Set(['tomato', 'basil', 'sausage', 'ham', 'chicken', 'beef', 'pork', 'sous-vide', 'bbq', 'mozzarella', 'garlic', 'paprika', 'pepper', 'marinara', 'pesto',
                                        'mushrooms', 'cheddar', 'parmesan', 'dor-blue', 'pepperoni', 'salami', 'onion', 'cucumber', 'parsley']);
let pizzaMenu = [
    {
        name: "Americano",
        ingredients: new Set(['tomato', 'BBQ', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara']),
        price:5,
        calories:291,
        photo_name:"./resources/img/pizza/americano.png"
    },
    {
        name: "Americano XXL",
        ingredients: new Set(['tomato', 'BBQ', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara']),
        price:10,
        calories:291,
        photo_name:"./resources/img/pizza/americano-xxl.png"
    },
    {
        name: "Bavarian",
        ingredients: new Set(['tomato', 'BBQ', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:7,
        calories:268,
        photo_name:"./resources/img/pizza/bavarian.png"
    },
    {
        name: "Bavarian XXL",
        ingredients: new Set(['tomato', 'BBQ', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:13,
        calories:268,
        photo_name:"./resources/img/pizza/bavarian-xxl.png"
    },
    {
        name: "Berlusconi XXL",
        ingredients: new Set(['salami', 'chicken','mushrooms','mozzarella', 'garlic', 'parsley', 'basil']),
        price:15,
        calories:301,
        photo_name:"./resources/img/pizza/berlusconi-xxl.png"
    },
    {
        name: "Calzzone",
        ingredients: new Set(['tomato', 'BBQ', 'ham','mushrooms','mozzarella', 'parmesan']),
        price:5,
        calories:261,
        photo_name:"./resources/img/pizza/calzzone.png"
    },
    {
        name: "Carbonara",
        ingredients: new Set(['tomato', 'BBQ', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:7,
        calories:283,
        photo_name:"./resources/img/pizza/carbonara.png"
    },
    {
        name: "Carbonara XXL",
        ingredients: new Set(['tomato', 'BBQ', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:15,
        calories:283,
        photo_name:"./resources/img/pizza/carbonara-xxl.png"
    },
    {
        name: "Goro",
        ingredients: new Set(['tomato', 'ham','mushrooms','cheddar', 'beef', 'pesto']),
        price:6,
        calories:294,
        photo_name:"./resources/img/pizza/cheese.png"
    },
    {
        name: "Diablo",
        ingredients: new Set(['tomato', 'mozzarella', 'pepperoni', 'garlic', 'pepper', 'paprika', 'basil', 'marinara']),
        price:5,
        calories:281,
        photo_name:"./resources/img/pizza/diablo.png"
    },
    {
        name: "Diablo XXL",
        ingredients: new Set(['tomato', 'mozzarella', 'pepperoni', 'garlic', 'pepper', 'paprika', 'basil', 'marinara']),
        price:10,
        calories:281,
        photo_name:"./resources/img/pizza/diablo-xxl.png"
    },
    {
        name: "Gurmeo",
        ingredients: new Set(['tomato', 'BBQ', 'ham','mushrooms', 'sausage', 'dor-blue', 'pork']),
        price:6,
        calories:284,
        photo_name:"./resources/img/pizza/gurmeo.png"
    },
    {
        name: "Gurmeo XXL",
        ingredients: new Set(['tomato', 'BBQ', 'ham','mushrooms', 'sausage', 'dor-blue', 'pork']),
        price:13,
        calories:284,
        photo_name:"./resources/img/pizza/gurmeo-xxl.png"
    },
    {
        name: "Hunters` treasure",
        ingredients: new Set(['BBQ', 'ham','sausage','mozzarella', 'pepperoni', 'beef', 'parsley', 'basil', 'pork']),
        price:7,
        calories:294,
        photo_name:"./resources/img/pizza/hunter.png"
    },
    {
        name: "Hunter`s treasure XXL",
        ingredients: new Set(['BBQ', 'ham','sausage','mozzarella', 'pepperoni', 'beef', 'parsley', 'basil', 'pork']),
        price:6,
        calories:291,
        photo_name:"./resources/img/pizza/hunter-xxl.png"
    },
    {
        name: "Mafia",
        ingredients: new Set(['tomato', 'BBQ', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:8,
        calories:271,
        photo_name:"./resources/img/pizza/mafia.png"
    },

    {
        name: "Mafia XXL",
        ingredients: new Set(['tomato', 'BBQ', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:18,
        calories:281,
        photo_name:"./resources/img/pizza/mafia-xxl.png"
    },

    {
        name: "Margarita",
        ingredients: new Set(['tomato', 'ham', 'parsley', 'basil', 'marinara']),
        price:6,
        calories:251,
        photo_name:"./resources/img/pizza/margarita.png"
    },

    {
        name: "Polo",
        ingredients: new Set(['tomato', 'BBQ', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara']),
        price:7,
        calories:261,
        photo_name:"./resources/img/pizza/polo.png"
    }
];
